package com.example.nobitex.Fragment

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.example.nobitex.Activity.Log_in_activity
import com.example.nobitex.Activity.activity_account.Security_activity

import com.example.nobitex.R
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_account_fragment.*

/**
 * A simple [Fragment] subclass.
 */
class Account_fragment : Fragment() {



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_account_fragment, container, false)


        val sing_in = view.findViewById<TextView>(R.id.account_fragment_log_in_or_sing_in_id)
        val security_btn = view.findViewById<TextView>(R.id.account_fragment_security_id)

        // باز کردن صفحه ثبت نام و ورود
        sing_in.setOnClickListener{
            val intent = Intent(context,Log_in_activity::class.java)
            startActivity(intent)
        }

        // باز کردن صفحه امنیت
        security_btn.setOnClickListener {
            startActivity(Intent(context,Security_activity::class.java))
        }


        return view
    }

}
