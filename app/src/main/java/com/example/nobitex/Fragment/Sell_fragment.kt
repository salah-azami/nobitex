package com.example.nobitex.Fragment

import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import com.example.nobitex.R
import kotlinx.android.synthetic.main.fragment_sell_fragment.view.*

class Sell_fragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_sell_fragment, container, false)


        // order popup menu is created
        val limitorder = view.findViewById<TextView>(R.id.trade_fragment_limited_order_id)
        limitorder.setOnClickListener {
            val popupmenu = PopupMenu(context,it)
            popupmenu.menuInflater.inflate(R.menu.popup_order,popupmenu.menu)
            popupmenu.show()
            popupmenu.setOnMenuItemClickListener {
                when(it.itemId){
                    R.id.popup_limit_order_id -> {
                        view.trad_sell_fragment_frame_layout_id.visibility = View.GONE
                        view.trad_sell_fragment_linear_layout_id.visibility = View.VISIBLE
                        true
                    }
                    R.id.popup_fast_order_id -> {
                        view.trad_sell_fragment_frame_layout_id.visibility = View.VISIBLE
                        view.trad_sell_fragment_linear_layout_id.visibility = View.GONE
                         true
                    }
                    else -> false

                }
            }
        }


        return view
    }
}
