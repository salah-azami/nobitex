package com.example.nobitex.Fragment

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.viewpager.widget.ViewPager
import com.example.nobitex.Activity.Currency_pair_activity
import com.example.nobitex.Activity.Open_order_activity
import com.example.nobitex.Activity.Order_history_activity
import com.example.nobitex.Adapter.Market_adapter
import com.example.nobitex.Adapter.Trades_adapter

import com.example.nobitex.R
import com.google.android.material.tabs.TabLayout
import org.w3c.dom.Text

/**
 * A simple [Fragment] subclass.
 */
class Trades_fragment() : Fragment() {

    lateinit var viewpager: ViewPager
    lateinit var tabs: TabLayout

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
       val view =  inflater.inflate(R.layout.fragment_trades_fragment, container, false)
        viewpager = view.findViewById(R.id.trade_fragment_view_pager_id)
        tabs = view.findViewById(R.id.trad_fragment_table_id)

        // view pager is connected to tab layout
        val fragmentAdapter  = Trades_adapter(childFragmentManager)
        viewpager.adapter = fragmentAdapter
        tabs.setupWithViewPager(viewpager)
        tabs.setTabTextColors(Color.WHITE, Color.GREEN)
        // scroll view pager is disabled
        viewpager.beginFakeDrag()


        // open currency pair activity
        val currencypairbtn = view.findViewById<TextView>(R.id.tad_fragment_change_coin_id)
        currencypairbtn.setOnClickListener {
            val intent = Intent(context,Currency_pair_activity::class.java)
            startActivity(intent)
        }

        // open orders activity
        val ordersbtn = view.findViewById<TextView>(R.id.trad_fragment_orders_id)
        ordersbtn.setOnClickListener {
            startActivity(Intent(context,Open_order_activity::class.java))
        }

        // open order history activity
        val orderhistorybtn = view.findViewById<TextView>(R.id.trade_fragment_history_id)
        orderhistorybtn.setOnClickListener {
            startActivity(Intent(context,Order_history_activity::class.java))
        }

        return view
    }


}
