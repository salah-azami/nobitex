package com.example.nobitex.Fragment

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.ViewPager
import com.example.nobitex.Adapter.Market_adapter

import com.example.nobitex.R
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_market_fragment.*

/**
 * A simple [Fragment] subclass.
 */
class Market_fragment : Fragment() {

    lateinit var viewpager: ViewPager
    lateinit var tabs: TabLayout

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
       val view = inflater.inflate(R.layout.fragment_market_fragment, container, false)

        // tab layout and view pager are connected to each other
        viewpager = view.findViewById(R.id.market_fragment_view_pager_id)
        tabs = view.findViewById(R.id.market_fragment_tabel_id)
       val fragmentAdapter  = Market_adapter(childFragmentManager)
        viewpager.adapter = fragmentAdapter
        tabs.setupWithViewPager(viewpager)
        tabs.setTabTextColors(Color.WHITE, Color.GREEN)

        return view

    }

}
