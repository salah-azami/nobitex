package com.example.nobitex.Fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.core.view.get
import com.example.nobitex.Activity.Balance_activity
import com.example.nobitex.Adapter.Custom_possession_adapter
import com.example.nobitex.Model.possession_list_model

import com.example.nobitex.R

/**
 * A simple [Fragment] subclass.
 */
class Possession_fragment : Fragment() {


    // لیستی از اطلاعاتی که قرار است منتقل شود
    val coin_image_list = arrayOf(
        R.drawable.trx,
        R.drawable.btc,
        R.drawable.eth,
        R.drawable.ltc,
        R.drawable.usdt,
        R.drawable.xrp,
        R.drawable.bch,
        R.drawable.bnb,
        R.drawable.eos,
        R.drawable.xlm,
        R.drawable.etc,
        R.drawable.trx
    )
    val coin_name_list = arrayOf(
        " تومان",
        "بیت کوین",
        "اتریوم",
        "(لایت کوین) LTC",
        "تتر",
        "ریپل",
        "بیت کوین کش",
        "بایننس",
        "آیاس",
        " استلار",
        "اتریوم کلاسیک",
        "ترون"
    )
    val coin_sing_list = arrayOf(
        " IRT",
        "BTC",
        "ETH",
        "LTC",
        "USDT",
        "XRP",
        "BCH",
        "BNB",
        "EOS",
        "XLM",
        "ETC",
        "TRX"
    )
    val coin_fee_list = arrayOf(
        "0",
        "0.0004",
        "0.003",
        "0.001",
       " 1.19",
       " 0.25",
        "0.001",
        "0.001",
        "0.1",
        "0.01",
       " 0.01",
        "1"
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_possession_fragment, container, false)
        // coin list view is created
        val listview = view.findViewById<ListView>(R.id.possession_fragment_list_view_id)
        listview.adapter = Custom_possession_adapter(context!!)

        // با کلیک روی هر آیتم از لیست ویو اطلاعات آن به balance activity منتقل می شود
        listview.setOnItemClickListener { parent, view, position, id ->
         val intent = Intent(context,Balance_activity::class.java)
         intent.putExtra("coinname",coin_name_list[position])
         intent.putExtra("coinsing",coin_sing_list[position])
         intent.putExtra("coinimage",coin_image_list[position])
         intent.putExtra("coinfee",coin_fee_list[position])
         startActivity(intent)
        }

        return view

    }

}
