package com.example.nobitex.Activity.activity_account

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Switch
import android.widget.Toast
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import com.example.nobitex.Activity.MainActivity
import com.example.nobitex.R
import kotlinx.android.synthetic.main.activity_security_activity.*
import java.util.concurrent.Executors

class Security_activity : AppCompatActivity() {
    // فراخوانی متدهایی برای اثر انگشت
    private val TAG = MainActivity::getLocalClassName.toString()
    private lateinit var biometricManager: BiometricManager
    private lateinit var biometricprompt:BiometricPrompt
    private lateinit var promptinfo: BiometricPrompt.PromptInfo

    // متغییر های لازم برای share pref
    val PREF_NAME = "myprefs"
    var mypref:SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_security_activity)

        supportActionBar!!.hide()

        security_back_id.setOnClickListener {
            finish()
        }
        // فعال کردن اثر انگشت
        // فعال کردن اثر انگشت
        biometricManager = BiometricManager.from(this)
        val executor = ContextCompat.getMainExecutor(this)

        checkbiometricstatus(biometricManager)

        biometricprompt = BiometricPrompt(this,executor,
            object : BiometricPrompt.AuthenticationCallback(){
                override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                    super.onAuthenticationError(errorCode, errString)
                    showtoast("authentication error : $errString")
                }

                override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                    super.onAuthenticationSucceeded(result)
                    security_finger_switch_id.splitTrack = true

                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    showtoast("authentication failed")
                }
            })

        promptinfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle("شناسایی اثر انگشت")
            .setDescription("لطفا انگشت خود را بر روی حسگر اثر انگشت قرار دهید.")
            .setNegativeButtonText("بستن")
            .build()

        security_finger_switch_id.setOnCheckedChangeListener { buttonView, isChecked ->

            security_finger_switch_id.setOnClickListener {
                biometricprompt.authenticate(promptinfo)
                        if (isChecked){

                            // ذخیره کردن تغییرات با share pref
                            mypref = getSharedPreferences(PREF_NAME,0)
                            val editor:SharedPreferences.Editor = mypref!!.edit()
                            editor.putBoolean("message",true)
                            editor.commit()

                        }else{

                            // ذخیره کردن تغییرات با share pref
                            mypref = getSharedPreferences(PREF_NAME,0)
                            val editor:SharedPreferences.Editor = mypref!!.edit()
                            editor.putBoolean("message",false)
                            editor.commit()

                            biometricprompt.authenticate(promptinfo)
                }
            }
        }

        // اعمال تغییرات هنگام باز کردن اپلیکیشن
        val databack:SharedPreferences = getSharedPreferences(PREF_NAME,0)
        if (databack.contains("message")){
            val myswitch = databack.getBoolean("message",true)
            security_finger_switch_id.isChecked = myswitch
        }






    }
    // اورراید کردن فانکشن های مورد نیاز برای اجرا کردن اثر انگشت
    private fun showtoast(message:String){
        Toast.makeText(this,message,Toast.LENGTH_LONG).show()
    }

    fun checkbiometricstatus(biometricManager: BiometricManager){

        when(biometricManager.canAuthenticate()){
            BiometricManager.BIOMETRIC_SUCCESS ->
                Log.e(TAG,"success")

            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE ->
                Log.e(TAG,"no biometric in device")

            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE ->
                Log.e(TAG,"unavailable")

        }
    }

}