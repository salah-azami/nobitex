package com.example.nobitex.Activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.nobitex.R
import kotlinx.android.synthetic.main.activity_log_in_activity.*

class Log_in_activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in_activity)
        supportActionBar!!.hide()

        log_in_text_register_id.setOnClickListener {
            startActivity(Intent(this,Register_activity::class.java))
        }
    }
}