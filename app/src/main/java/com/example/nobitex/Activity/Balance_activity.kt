package com.example.nobitex.Activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.example.nobitex.R
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_balance_activity.*

class Balance_activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_balance_activity)
        supportActionBar!!.hide()
        // دریافت اطلاعات از possession fragment
        val coinfee = intent.extras!!.get("coinfee")
        val coinname = intent.extras!!.get("coinname")
        val coinsing = intent.extras!!.get("coinsing")
        val coinimage = intent.getIntExtra("coinimage",0)!!
        val coin_name = findViewById<TextView>(R.id.balance_coin_name_id)
        val coin_sing = findViewById<TextView>(R.id.balance_coin_sign_id)
        val coin_image = findViewById<CircleImageView>(R.id.balance_image_id)
        coin_name.text = coinname.toString()
        coin_sing.text = coinsing.toString()
        Picasso.get()
            .load(coinimage)
            .into(coin_image)

        balance_back_btn_id.setOnClickListener {
            finish()
        }
        // باز کردن withdraw activity و فرستادن اطلاعات برای آن
        balance_withdrawal_btn_id.setOnClickListener {
            val intent = Intent(this,Withdrawal_activity::class.java)
            intent.putExtra("coinname",coinname.toString())
            intent.putExtra("coinsing",coinsing.toString())
            intent.putExtra("coinfee",coinfee.toString())
            intent.putExtra("coinimage",coinimage)
            startActivity(intent)
        }
        //  باز کردن deposit activity وفرستادن اصلاعات برای آن
        balance_deposit_btn_id.setOnClickListener {
            val intent = Intent(this,Deposit_activity::class.java)
            intent.putExtra("coinsing",coinsing.toString())
            intent.putExtra("coinfee",coinfee.toString())
            intent.putExtra("coinimage",coinimage)
            startActivity(intent)
        }
    }
}