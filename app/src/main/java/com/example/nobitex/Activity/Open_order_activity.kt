package com.example.nobitex.Activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.nobitex.R
import kotlinx.android.synthetic.main.activity_open_order_activity.*

class Open_order_activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_open_order_activity)
        supportActionBar!!.hide()

        // activity closed
        open_order_back_btn_id.setOnClickListener {
            finish()
        }

        // activity history is active

        order_history_btn_id.setOnClickListener {
            startActivity(Intent(this,Order_history_activity::class.java))
            finish()
        }
    }
}