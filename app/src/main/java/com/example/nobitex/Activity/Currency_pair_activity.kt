package com.example.nobitex.Activity

import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.viewpager.widget.ViewPager
import com.example.nobitex.Adapter.Market_adapter
import com.example.nobitex.Fragment.Account_fragment
import com.example.nobitex.Fragment.Market_fragment
import com.example.nobitex.Fragment.Sell_fragment
import com.example.nobitex.Fragment.Trades_fragment
import com.example.nobitex.R
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_currency_pair_activity.*

class Currency_pair_activity : AppCompatActivity() {
    lateinit var viewpager: ViewPager
    lateinit var tabs: TabLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_currency_pair_activity)
        supportActionBar!!.hide()

        // tab layout and view pager are connected to each other
        viewpager = findViewById(R.id.currency_pair_view_pager_id)
        tabs = findViewById(R.id.currency_pair_tabel_id)
        val fragmentAdapter  = Market_adapter(supportFragmentManager)
        viewpager.adapter = fragmentAdapter
        tabs.setupWithViewPager(viewpager)
        tabs.setTabTextColors(Color.WHITE, Color.GREEN)

        // currency pair activity is closed
        val backbtn = findViewById<TextView>(R.id.currencu_pair_back_btn_id)
      backbtn.setOnClickListener {
        finish()
        }
    }
}