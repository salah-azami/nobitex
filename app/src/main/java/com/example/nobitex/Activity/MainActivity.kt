package com.example.nobitex.Activity

import android.app.KeyguardManager
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CancellationSignal
import android.util.Log
import android.widget.Switch
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.ActionBar
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.getSystemService
import androidx.fragment.app.Fragment
import com.example.nobitex.Fragment.Account_fragment
import com.example.nobitex.Fragment.Market_fragment
import com.example.nobitex.Fragment.Possession_fragment
import com.example.nobitex.Fragment.Trades_fragment
import com.example.nobitex.R
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_security_activity.*
import java.util.concurrent.Executors

class MainActivity : AppCompatActivity() {

    lateinit var toolbar: ActionBar

        val bottom = BottomNavigationView.OnNavigationItemSelectedListener{
            when(it.itemId){
                R.id.bottom_account_id -> {

                    val fragment = Account_fragment()
                    openfragment(fragment)

                    return@OnNavigationItemSelectedListener true
                }
                R.id.bottom_possession_id -> {
                    val fragment = Possession_fragment()
                    openfragment(fragment)
                  return@OnNavigationItemSelectedListener true
                }

                R.id.bottom_trades_id -> {
                    val fragment = Trades_fragment()
                    openfragment(fragment)
                    return@OnNavigationItemSelectedListener true
                }

                R.id.bottom_markets_id -> {
                    val fragment = Market_fragment()
                    openfragment(fragment)
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

    fun openfragment(fragment:Fragment){

      supportFragmentManager
             .beginTransaction()
             .replace(R.id.home_page_layout_id,fragment)
             .commit()
    }

    // فراخوانی متدهایی برای اثر انگشت
    private val TAG = MainActivity::getLocalClassName.toString()
    private lateinit var biometricManager: BiometricManager
    private lateinit var biometricprompt:BiometricPrompt
    private lateinit var promptinfo: BiometricPrompt.PromptInfo



    @RequiresApi(Build.VERSION_CODES.P)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar!!.hide()

        toolbar = supportActionBar!!

        val bottomnavigation:BottomNavigationView = findViewById(R.id.home_page_bottom_id)

        bottomnavigation.setOnNavigationItemSelectedListener(bottom)

        // فعال کردن اثر انگشت
        biometricManager = BiometricManager.from(this)
        val executor = ContextCompat.getMainExecutor(this)

        checkbiometricstatus(biometricManager)

        biometricprompt = BiometricPrompt(this,executor,
        object : BiometricPrompt.AuthenticationCallback(){
            override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                super.onAuthenticationError(errorCode, errString)
                showtoast("authentication error : $errString")
            }

            override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                super.onAuthenticationSucceeded(result)
            }

            override fun onAuthenticationFailed() {
                super.onAuthenticationFailed()
                showtoast("authentication failed")
            }
        })

        promptinfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle("شناسایی اثر انگشت")
            .setDescription("لطفا انگشت خود را بر روی حسگر اثر انگشت قرار دهید.")
            .setNegativeButtonText("بستن")
            .build()



//        biometricprompt.authenticate(promptinfo)

    }

    // اورراید کردن فانکشن های مورد نیاز برای اجرا کردن اثر انگشت
    private fun showtoast(message:String){
        Toast.makeText(this,message,Toast.LENGTH_LONG).show()
    }


    fun checkbiometricstatus(biometricManager: BiometricManager){

        when(biometricManager.canAuthenticate()){
            BiometricManager.BIOMETRIC_SUCCESS ->
                Log.e(TAG,"success")

            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE ->
                Log.e(TAG,"no biometric in device")

            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE ->
                Log.e(TAG,"unavailable")

        }
    }





}
