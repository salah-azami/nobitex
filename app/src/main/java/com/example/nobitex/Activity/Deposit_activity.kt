package com.example.nobitex.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.example.nobitex.R
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.android.synthetic.main.activity_deposit_activity.*
import java.lang.Exception

class Deposit_activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_deposit_activity)

        supportActionBar!!.hide()

        deposit_back_btn_id.setOnClickListener {
            finish()
        }

        // تبدیل کردن تکست به QRcode
        val barcodeimage = findViewById<ImageView>(R.id.deposit_barcode_image_id)
        val addresstext = findViewById<TextView>(R.id.deposit_address_text_id)
        if (addresstext != null){
            try {
                val encoder = BarcodeEncoder()
                val bitmap = encoder.encodeBitmap(addresstext.text.toString(), BarcodeFormat.QR_CODE,500,500)
                barcodeimage.setImageBitmap(bitmap)
            }catch (e:Exception){
                e.printStackTrace()
            }
        }

        // دریافت داده ها از balance activity
        val coinfee = intent.extras!!.get("coinfee")
        val coinsing = intent.extras!!.get("coinsing")
        val coinimage = intent.getIntExtra("coinimage",0)
        deposit_circle_image_id.setImageResource(coinimage)
        deposit_text_sing_id.text = coinsing.toString()
        deposit_sing_address_id.text = coinsing.toString()

    }
}