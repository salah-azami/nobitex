package com.example.nobitex.Activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.text.Editable
import android.view.View
import android.widget.EditText
import com.example.nobitex.R
import com.google.zxing.integration.android.IntentIntegrator
import com.google.zxing.integration.android.IntentResult
import kotlinx.android.synthetic.main.activity_withdrawal_activity.*

class Withdrawal_activity : AppCompatActivity() {

    var scannedresult:String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_withdrawal_activity)

        supportActionBar!!.hide()
        // دریافت داده ها از balance activity
        val coinfee = intent.extras!!.get("coinfee")
        val coinsing = intent.extras!!.get("coinsing")
        val coinimage = intent.getIntExtra("coinimage",0)

        // وارد کردن داده ها دریافت شده در فیلد های مربوط به ارز
        withdraw_value_fee_coin_id.text = coinfee.toString()
        withdraw_sing_text_id.text = coinsing.toString()
        withdraw_sing_get_value_id.text = coinsing.toString()
        withdraw_sing_fee_text_id.text = coinsing.toString()
        withdraw_image_id.setImageResource(coinimage)
        // بستن activity
        withdraw_back_btn_id.setOnClickListener {
            finish()
        }
        // باز کردن دروبین گوشی برای اسکن کردن
        withdraw_q_r_btn_id.setOnClickListener {
          run {
              IntentIntegrator(this@Withdrawal_activity).initiateScan()
          }
        }
    }
    // باز کردن دوربین گوشی، اسکن QRcode و فرستادن داده ها به withdraw activity
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        var result:IntentResult = IntentIntegrator.parseActivityResult(requestCode,resultCode,data)

        if (result != null){

            if (result.contents != null){

                scannedresult = result.contents
                withdraw_Q_R_text_view_id.text = scannedresult


            }else{

                withdraw_Q_R_text_view_id.text = ""
            }
        }else{
            super.onActivityResult(requestCode, resultCode, data)
        }

    }
    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        outState.putString("scannedresult", scannedresult)
        super.onSaveInstanceState(outState, outPersistentState)
    }
    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        savedInstanceState.let {
            scannedresult = it.getString("scannedresult")!!
            withdraw_Q_R_text_view_id.text = scannedresult
        }
    }
}