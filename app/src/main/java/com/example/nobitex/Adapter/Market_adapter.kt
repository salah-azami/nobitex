package com.example.nobitex.Adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.nobitex.Fragment.IRT_fragment
import com.example.nobitex.Fragment.USDT_fragment

class Market_adapter(fragmentManager: FragmentManager):FragmentPagerAdapter(fragmentManager) {
    override fun getItem(position: Int): Fragment {
        return when(position){
            0 -> {
                USDT_fragment()
            }else -> {

                    return IRT_fragment()

            }

        }
    }

    override fun getCount(): Int {
      return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
            0 -> "USDT"

            else -> "IRT"
        }
    }
}