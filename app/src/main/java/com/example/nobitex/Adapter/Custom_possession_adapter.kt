package com.example.nobitex.Adapter

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.BaseAdapter
import android.widget.TextView
import com.example.nobitex.Model.possession_list_model
import com.example.nobitex.R
import de.hdodenhof.circleimageview.CircleImageView

class Custom_possession_adapter(
    val context:Context
):BaseAdapter(){

//    val listofcoin = arrayOf(
//        possession_list_model(" (تومان) IRT",R.drawable.trx,"0"),
//        possession_list_model("(بیت کوین) BTC",R.drawable.BTC,"0"),
//        possession_list_model("(اتریوم) ETH",R.drawable.ETH,"0"),
//        possession_list_model("(لایت کوین) LTC",R.drawable.LTC,"0"),
//        possession_list_model("(تتر) USDT",R.drawable.usdt,"0"),
//        possession_list_model("(ریپل) XRP",R.drawable.XRP,"0"),
//        possession_list_model("(بیت کوین کش) BCH",R.drawable.BCH,"0"),
//        possession_list_model("(بایننس) BNB",R.drawable.BNB,"0"),
//        possession_list_model("(آیاس) EOS",R.drawable.eos,"0"),
//        possession_list_model(" (استلار) XLM",R.drawable.xlm,"0"),
//        possession_list_model("(اتریوم کلاسیک) ETC",R.drawable.etc,"0"),
//        possession_list_model("(ترون) TRX",R.drawable.trx,"0")
//    )

    val coin_name_list = arrayOf(
        " (تومان) IRT",
        "(بیت کوین) BTC",
        "(اتریوم) ETH",
        "(لایت کوین) LTC",
        "(تتر) USDT",
        "(ریپل) XRP",
        "(بیت کوین کش) BCH",
        "(بایننس) BNB",
        "(آیاس) EOS",
        " (استلار) XLM",
        "(اتریوم کلاسیک) ETC",
        "(ترون) TRX"
    )

    val coin_image_list = arrayOf(
        R.drawable.trx,
        R.drawable.btc,
        R.drawable.eth,
        R.drawable.ltc,
        R.drawable.usdt,
        R.drawable.xrp,
        R.drawable.bch,
        R.drawable.bnb,
        R.drawable.eos,
        R.drawable.xlm,
        R.drawable.etc,
        R.drawable.trx
    )

    val coin_value_list = arrayOf(
        "0",
        "0",
        "0",
        "0",
        "0",
        "0",
        "0",
        "0",
        "0",
        "0",
        "0",
        "0"
    )

    private val mcontext:Context

    init {
        mcontext = context

    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
    val Layout = LayoutInflater.from(context)
    val view = Layout.inflate(R.layout.custom_possession_list_view,parent,false)
    val namecoin = view.findViewById<TextView>(R.id.custom_possession_text_coin_id)
    val imagecoin = view.findViewById<CircleImageView>(R.id.custom_possession_circle_image_id)
    val valuecoin = view.findViewById<TextView>(R.id.custom_possession_value_id)

    namecoin.text = coin_name_list.get(position)
    imagecoin.setImageResource(coin_image_list.get(position))
    valuecoin.text = coin_value_list.get(position)

    return view
    }

    override fun getItem(position: Int): Any {
        return coin_image_list.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return coin_name_list.count()
    }


}