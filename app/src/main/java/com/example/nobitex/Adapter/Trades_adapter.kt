package com.example.nobitex.Adapter

import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.nobitex.Fragment.Buy_fragment
import com.example.nobitex.Fragment.Sell_fragment
import com.example.nobitex.R
import kotlinx.android.synthetic.main.fragment_sell_fragment.view.*

class Trades_adapter(fm:FragmentManager):FragmentPagerAdapter(fm) {

        val view:View? = null
    override fun getItem(position: Int): Fragment {

        return when (position) {

            0 -> Sell_fragment()

            else -> Buy_fragment()
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {


        return when(position){
            0 -> "فروش"
            else -> "خرید"
        }
    }


}

