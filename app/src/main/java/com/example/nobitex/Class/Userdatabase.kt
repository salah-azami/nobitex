package com.example.nobitex.Class

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.nobitex.`interface`.Userdao

@Database(entities = arrayOf(User::class) ,version = 1)
abstract class Userdatabase : RoomDatabase(){

    abstract fun user() :Userdao

    companion object{
        private var instance : Userdatabase? = null
        val DB_NAME = "nobi_db"

        fun getinstance(context: Context) : Userdatabase{
            if (instance == null){
                instance = Room.databaseBuilder(context , Userdatabase::class.java , DB_NAME)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build()
            }
            return instance!!

        }
    }

}