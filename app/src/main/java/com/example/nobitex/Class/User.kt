package com.example.nobitex.Class

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "nobi_tbl")
data class User(@PrimaryKey val id:Int,
                @ColumnInfo val email:String? = "" ,
                @ColumnInfo val password:String){

    constructor(email: String? , password: String): this(0,email, password)
}
