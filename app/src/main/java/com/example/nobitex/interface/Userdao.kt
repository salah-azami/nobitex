package com.example.nobitex.`interface`

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.nobitex.Class.User

@Dao
interface Userdao {

    @Insert
    fun createuser(user:User)

    @Query("SELECT * FROM nobi_tbl")
    fun getuser():List<User>

    @Delete
    fun deleteuser(user: User)

    @Update
    fun updateuser(user: User)

    @Query("DELETE FROM nobi_tbl")
    fun deleteall()
}